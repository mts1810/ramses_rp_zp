!################################################################
!################################################################
!################################################################
!################################################################
subroutine thermal_feedback(ilevel)
  use pm_commons
  use amr_commons
  implicit none
  integer::ilevel
  !------------------------------------------------------------------------
  ! This routine computes the thermal energy, the kinetic energy and 
  ! the metal mass dumped in the gas by stars (SNII, SNIa, winds).
  ! This routine is called every fine time step.
  !------------------------------------------------------------------------
  ! Sim scales not used in this routine... 
  !  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp)::t0,scale,dx_min,vsn,rdebris,ethermal
  integer::igrid,jgrid,ipart,jpart,next_part
  integer::i,ig,ip,npart1,npart2,icpu,nx_loc
  real(dp),dimension(1:3)::skip_loc
  integer,dimension(1:nvector),save::ind_grid,ind_part,ind_grid_part

  if(numbtot(1,ilevel)==0)return ! Check number of grids at ilevel
  if(verbose)write(*,111)ilevel

  ! Gather star particles only.
#if NDIM==3
  ! Loop over cpus
  do icpu=1,ncpu
     igrid=headl(icpu,ilevel)
     ig=0
     ip=0
     ! Loop over grids
     do jgrid=1,numbl(icpu,ilevel)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0
        
        ! Count star particles
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if(idp(ipart).lt.0.and.tp(ipart).ne.0)then
                 npart2=npart2+1
              endif
              ipart=next_part  ! Go to next particle
           end do
        endif
        
        ! Gather star particles
        if(npart2>0)then        
           ig=ig+1
           ind_grid(ig)=igrid
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              ! Select only star particles
              if(idp(ipart).lt.0.and.tp(ipart).ne.0)then
                 if(ig==0)then
                    ig=1
                    ind_grid(ig)=igrid
                 end if
                 ip=ip+1
                 ind_part(ip)=ipart
                 ind_grid_part(ip)=ig   
              endif
              if(ip==nvector)then
                 call feedbk(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
                 ip=0
                 ig=0
              end if
              ipart=next_part  ! Go to next particle
           end do
           ! End loop over particles
        end if
        igrid=next(igrid)   ! Go to next grid
     end do
     ! End loop over grids
     if(ip>0)call feedbk(ind_grid,ind_part,ind_grid_part,ig,ip,ilevel)
  end do 
  ! End loop over cpus

#endif

111 format('   Entering thermal_feedback for level ',I2)

end subroutine thermal_feedback
!################################################################
!################################################################
!################################################################
!################################################################
subroutine feedbk(ind_grid,ind_part,ind_grid_part,ng,np,ilevel)
  use amr_commons ! This is where h2_frac is defined
  use pm_commons
  use hydro_commons
  implicit none
  integer::ng,np,ilevel
  integer,dimension(1:nvector)::ind_grid
  integer,dimension(1:nvector)::ind_grid_part,ind_part
  !-----------------------------------------------------------------------
  ! This routine is called by subroutine thermal_feedback. Each debris
  ! particle dumps mass, momentum and energy in the nearest grid cell
  ! using array uold.
  !-----------------------------------------------------------------------
  integer::i,j,idim,nx_loc
  real(kind=8)::RandNum
  real(dp)::SN_BOOST,mstar,dx_min,vol_min, ctv
  real(dp)::xxx,mmm,t0,ESN,mejecta,zloss,z3loss,EBNSM,rploss,m_rp_ejecta,EtwoSN !MTS (EtwoSN,rploss)
  real(dp)::ERAD,RAD_BOOST,tauIR,eta_sig,current_time
  real(dp)::sigma_d,delta_x,tau_factor,rad_factor
  real(dp)::dx,dx_loc,scale,vol_loc,birth_time
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  logical::error
  ! Grid based arrays
  real(dp),dimension(1:nvector,1:ndim),save::x0
  integer ,dimension(1:nvector),save::ind_cell
  integer ,dimension(1:nvector,1:threetondim),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim),save::nbors_father_grids
  ! Particle based arrays
  integer,dimension(1:nvector),save::igrid_son,ind_son
  integer,dimension(1:nvector),save::list1
  logical,dimension(1:nvector),save::ok
  real(dp),dimension(1:nvector),save::mloss,mzloss,mz3loss,ethermal,ekinetic,dteff,m_rp_loss!MTS(m_rp_loss)
  real(dp),dimension(1:nvector,1:ndim),save::x
  integer ,dimension(1:nvector,1:ndim),save::id,igd,icd
  integer ,dimension(1:nvector),save::igrid,icell,indp,kg
  real(dp),dimension(1:3)::skip_loc
!  integer ::idelay
!#ifdef SOLVERhydro
!  integer ::imetal=6
!#endif
!#ifdef SOLVERmhd
!  integer ::imetal=9
!#endif

! RS - handled in read_hyro_params.f90
!  if(metal)then
!     idelay=imetal+1
!  else
!     idelay=6
!  endif
  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Mesh spacing in that level
  dx=0.5D0**ilevel
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  if(ndim>0)skip_loc(1)=dble(icoarse_min)
  if(ndim>1)skip_loc(2)=dble(jcoarse_min)
  if(ndim>2)skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_loc=dx*scale
  vol_loc=dx_loc**ndim
  dx_min=(0.5D0**nlevelmax)*scale
  vol_min=dx_min**ndim

  ! Massive star lifetime from Myr to code units
  t0=10.*1d6*(365.*24.*3600.)/scale_t
  current_time=t

  ! Minimum star particle mass
  mstar=n_star/(scale_nH*aexp**3)*vol_min

  ! Compute stochastic boost to account for target GMC mass
  SN_BOOST=MAX(mass_gmc*2d33/(scale_d*scale_l**3)/mstar,1d0)

  ! Type II supernova specific energy from cgs to code units
  ESN=1d51/(10.*2d33)/scale_v**2 ! RS - We'll assume E/mass is the same for regular and PISN 

  ! Life time radiation specific energy from cgs to code units
  ERAD=1d53/(10.*2d33)/scale_v**2

#if NDIM==3
  ! Lower left corner of 3x3x3 grid-cube
  do idim=1,ndim
     do i=1,ng
        x0(i,idim)=xg(ind_grid(i),idim)-3.0D0*dx
     end do
  end do

  ! Gather 27 neighboring father cells (should be present anytime !)
  do i=1,ng
     ind_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(ind_cell,nbors_father_cells,nbors_father_grids,ng,ilevel)

  ! Rescale position at level ilevel
  do idim=1,ndim
     do j=1,np
        x(j,idim)=xp(ind_part(j),idim)/scale+skip_loc(idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)-x0(ind_grid_part(j),idim)
     end do
  end do
  do idim=1,ndim
     do j=1,np
        x(j,idim)=x(j,idim)/dx
     end do
  end do

  ! Check for illegal moves
  error=.false.
  do idim=1,ndim
     do j=1,np
        if(x(j,idim)<=0.0D0.or.x(j,idim)>=6.0D0)error=.true.
     end do
  end do
  if(error)then
     write(*,*)'problem in sn2'
     write(*,*)ilevel,ng,np
     stop
  end if

  ! NGP at level ilevel
  do idim=1,ndim
     do j=1,np
        id(j,idim)=x(j,idim)
     end do
  end do

   ! Compute parent grids
  do idim=1,ndim
     do j=1,np
        igd(j,idim)=id(j,idim)/2
     end do
  end do
  do j=1,np
     kg(j)=1+igd(j,1)+3*igd(j,2)+9*igd(j,3)
  end do
  do j=1,np
     igrid(j)=son(nbors_father_cells(ind_grid_part(j),kg(j)))
  end do

  ! Check if particles are entirely in level ilevel
  ok(1:np)=.true.
  do j=1,np
     ok(j)=ok(j).and.igrid(j)>0
  end do

  ! Compute parent cell position
  do idim=1,ndim
     do j=1,np
        if(ok(j))then
           icd(j,idim)=id(j,idim)-2*igd(j,idim)
        end if
     end do
  end do
  do j=1,np
     if(ok(j))then
        icell(j)=1+icd(j,1)+2*icd(j,2)+4*icd(j,3)
     end if
  end do

  ! Compute parent cell adresses
  do j=1,np
     if(ok(j))then
        indp(j)=ncoarse+(icell(j)-1)*ngridmax+igrid(j)
     end if
  end do

  ! Compute individual time steps
  do j=1,np
     if(ok(j))then
        if(levelp(ind_part(j))>=ilevel)then
           dteff(j)=dtnew(levelp(ind_part(j)))
        else
           dteff(j)=dtold(levelp(ind_part(j)))
        end if
     endif
  end do

  ! Reset ejected mass, metallicity, thermal energy
  do j=1,np
     if(ok(j))then
        m_rp_loss(j)=0d0 ! MTS
        mloss(j)=0d0
        mzloss(j)=0d0
        if (prist_gas_fraction) mz3loss(j)=0d0 ! RS - needed for iprimordz
        ethermal(j)=0d0
     endif
  end do

  ! Compute stellar mass loss and thermal feedback due to supernovae
  if(f_w==0)then ! Mean's we're not carrying any of the ISM gas along with the SNe - RS
     do j=1,np
        if(ok(j))then
           birth_time=tp(ind_part(j))
           ! Make sure that we don't count feedback twice
           if(birth_time.lt.(current_time-t0).and.birth_time.ge.(current_time-t0-dteff(j)))then
              ! Stellar mass loss
!!$              mejecta=eta_sn*mp(ind_part(j))
              mejecta = 10.0 * 2d33/(scale_d*scale_l**3)
              if (mejecta > mp(ind_part(j))) print *,"feedbk: ERROR - mass loss in SN > mp", mejecta, mp(ind_part(j))
              mloss(j)=mloss(j)+mejecta/vol_loc ! RS- Summing up mloss (in units of density)
              ! Thermal energy
              ethermal(j)=ethermal(j)+mejecta*ESN/vol_loc
              ! Metallicity
              if(metal)then
                 ! yield - fraction of star particle that is transformed into Z
                 ! (1d0-yield)*zp(ind_part(j)) - part of the star particle that is "already" Z
                 zloss=yield+(1d0-yield)*zp(ind_part(j)) ! zloss is the total Z of ejecta
                 mzloss(j)=mzloss(j)+mejecta*zloss/vol_loc ! RS - Sum up mzloss (in density units)
                 if(prist_gas_fraction) then
                    ! Rick Sarmento
                    ! We're tracking iprist and iprimordz: pristine fraction & primordial Z
                    ! For iprist, we'll let the increase in the cell's density be the factor
                    ! by which the pristine fraction is reduced. iprist tracks the cell's
                    ! pristine density, so when we update the cell's density with the
                    ! mass of the ejecta, and later use that density to convert iprist
                    ! to a mass-fraction, it will be scaled by the new density: rho_old/rho_new
                    ! where rho_new = rho_old + mzloss
!!$                    print *,"feedbk -- NO GAS CARRIED. SN Mass only"

                    ! For iprimordz, only count the fraction of metals created by primordial stars
                    ! So, there is only a yield of Z due to the SN... no old Z mass to carry along
                    z3loss = yield 
                    ! Sum up the Z mass due to POP III stars
                    mz3loss(j)=mz3loss(j)+mejecta*z3loss*pfp(ind_part(j))/vol_loc 
                    ! For testing - let the mz3loss exactly track mzloss ... ensure
                    ! that it is tracking exactly... 
!!$                    mz3loss(j)=mz3loss(j)+mejecta*zloss/vol_loc ! FOR TEST ONLY - should track mzloss
                 endif
              endif
              ! Reduce star particle mass
              mp(ind_part(j))=mp(ind_part(j))-mejecta
!!              print *,"feedbk: remaining particle mass: ",mp(ind_part(j))
              idp(ind_part(j))=-idp(ind_part(j)) ! Reverse index to tag exploded stars
              ! Boost SNII energy and depopulate accordingly
              if(SN_BOOST>1d0)then
                 call ranf(localseed,RandNum)
                 if(RandNum<1d0/SN_BOOST)then
                    mloss(j)=SN_BOOST*mloss(j)
                    mzloss(j)=SN_BOOST*mzloss(j)
                    if(prist_gas_fraction) then
                       mz3loss(j)=SN_BOOST*mz3loss(j) ! Do I want to do this? RS
                       print *,"feedback: BOOSTING mz3loss. SN_BOOST",SN_BOOST
                    endif
                    ethermal(j)=SN_BOOST*ethermal(j)
                 else
                    mloss(j)=0d0
                    mzloss(j)=0d0
                    if(prist_gas_fraction) then
                       mz3loss(j)=0d0
                       print *,"feedbk: - reseting mz3loss = 0 because of RandNum!"
                    endif
                    ethermal(j)=0d0
                 endif
              endif
           endif
        end if
     end do ! end loop over all the star particles
  endif ! end if f_w = 0

  ! Update hydro variables due to feedback

  ! For IR radiation trapping,
  ! we use the cell resolution to estimate the column density of gas
  delta_x=200*3d18
  if(metal)then
     tau_factor=kappa_IR*delta_x*scale_d/0.02
  else
     tau_factor=kappa_IR*delta_x*scale_d*z_ave
  endif
  rad_factor=ERAD/ESN
  do j=1,np
     if(ok(j))then

        ! Infrared photon trapping boost
        if(metal)then
           tauIR=tau_factor*max(uold(indp(j),imetal),smallr)
        else
           tauIR=tau_factor*max(uold(indp(j),1),smallr)
        endif
        RAD_BOOST=rad_factor*(1d0-exp(-tauIR))
        
        ! Specific kinetic energy of the star
        ekinetic(j)=0.5*(vp(ind_part(j),1)**2 &
             &          +vp(ind_part(j),2)**2 &
             &          +vp(ind_part(j),3)**2)
        ! Update hydro variable in NGP cell
        ! Rick Sarmento - Note that we're updating the cell's
        ! density and other parms BEFORE making the pristine
        ! fraction computation (called in amr_step).
        ! So when iprist (which was scaled with the pre-SNe cell
        ! density) is updated, it'll be reduced because of the addition
        ! of the new mass to the cell. This is an approximation for the
        ! change in pristine fraction.

        ! Ensure the v_t isn't artificially descreased with the increase in rho
        if (turbulent_velocity) then
!!$           print *, "feedback v_t before rho update ", uold(indp(j),iturbvel)/uold(indp(j),1) * scale_v/1.0d5
           ctv = uold(indp(j),iturbvel)/uold(indp(j),1)
        end if
        uold(indp(j),1)=uold(indp(j),1)+mloss(j)
        if (turbulent_velocity) then
           uold(indp(j),iturbvel) = ctv * uold(indp(j),1) ! Using the updated rho
!!$           print *, "feedback v_t after rho update ", uold(indp(j),iturbvel)/uold(indp(j),1) * scale_v/1.0d5
        end if
        uold(indp(j),2)=uold(indp(j),2)+mloss(j)*vp(ind_part(j),1)
        uold(indp(j),3)=uold(indp(j),3)+mloss(j)*vp(ind_part(j),2)
        uold(indp(j),4)=uold(indp(j),4)+mloss(j)*vp(ind_part(j),3)
        uold(indp(j),5)=uold(indp(j),5)+mloss(j)*ekinetic(j)+ &
             & ethermal(j)*(1d0+RAD_BOOST)
     endif
  end do

  ! Add metals
  ! RS - this is only for the f_w=0 case -- no ISM gas carried along.
  ! I think this should be in the if block for f_w == 0.0 since
  ! that's the only way mz(3)loss can be updated! - ask Yohan
  ! Although, the mloss array is initialized to 0.0 so this won't
  ! hurt if it is executed
  if(metal)then
     do j=1,np
        if(ok(j))then
           uold(indp(j),imetal)=uold(indp(j),imetal)+mzloss(j) ! RS - Update the Z 'density' for the cell
           if(prist_gas_fraction) then
              ! Nothing to be done for iprist... Since iprist is based on
              ! the original cell's density, and that density has increase due
              ! to SN ejecta, the pristine fraction will naturally decrease when
              ! scaled by the cell's new rho. (rho updated above)
              ! iprimordz - Z from primordial (POPIII) stars
              ! mz3loss is density of 'Z' generated by POPIII (primordial) stars
              uold(indp(j),iprimordz) = uold(indp(j),iprimordz) + mz3loss(j) 
!!$              if (uold(indp(j),imetal) > 1d-10) print *,"feedbk: Z/(1-P) ",uold(indp(j),imetal)/(uold(indp(j),1)-uold(indp(j),iprist))
           endif
        endif
     end do
  endif

  ! Add delayed cooling switch variable
  if(delayed_cooling)then
     do j=1,np
        if(ok(j))then
           uold(indp(j),idelay)=uold(indp(j),idelay)+mloss(j)
        endif
     end do
  endif

  ! Add  R-process !MTS
  if(rpgas)then
     do j=1,np
!        print *, "feedbk: before update uold(indp(j),irpgas): ", uold(indp(j),irpgas)
        uold(indp(j),irpgas)=uold(indp(j),irpgas)+m_rp_loss(j)
!        print *, "feedbk: after update uold(indp(j),irpgas): ", uold(indp(j),irpgas)
!        print *, "feedbk: m_rp_loss(j): ", m_rp_loss(j)
     end do
  endif

#endif
  
end subroutine feedbk


!################################################################
!################################################################
! Using a new particle field, tmp, check to determine if star particles (that have
! previously gone SN) are going to merge (in a NSB system).
! R. Sarmento, M. Safarzadeh 13 July 2017
!################################################################
!################################################################
subroutine check_for_NSB()
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
  integer::nNSB_tot_all
  real(kind=8)::RandNum
  integer,dimension(1:ncpu)::nNSB_icpu_all
  real(dp),dimension(:),allocatable::mNSB_all,rpNSB_all ! RS - arrays for particle pristine frac
  real(dp),dimension(:,:),allocatable::xNSB_all,vNSB_all
#endif
  !----------------------------------------------------------------------
  ! Description: This subroutine checks NSB events in cells
  !----------------------------------------------------------------------
  ! local constants
  integer::icpu,igrid,jgrid,npart1,npart2,ipart,jpart,next_part
  integer::nNSB,nNSB_tot,info,iNSB,ilevel,ivar
  integer,dimension(1:ncpu)::nNSB_icpu
  logical ::ok_free
!#ifdef SOLVERhydro
!  integer ::imetal=6
!#endif
!#ifdef SOLVERmhd
!  integer ::imetal=9
!#endif
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,t0,mass_load
  integer ,dimension(:),allocatable::indNSB,iNSB_myid
  real(dp),dimension(:),allocatable::mNSB,vol_gas,ekBlast,mloadNSB
  real(dp),dimension(:,:),allocatable::xNSB,vNSB,dq,vloadNSB
  integer ,dimension(:),allocatable::indNSB_tot,itemp
  real(dp),dimension(:),allocatable::mNSB_tot
  real(dp),dimension(:,:),allocatable::xNSB_tot,vNSB_tot
  real(dp),dimension(:),allocatable:: rpNSB,rploadNSB,rpNSB_tot ! MTS rpgas
  integer::isort

  if(.not. hydro)return
  if(ndim.ne.3)return

  if(verbose)write(*,*)'Entering check_for_NSB'
  
  ! This routine models a neutron star binary merger event
  ! rp_gas is updated...
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  !------------------------------------------------------
  ! Gather star particles eligible for a NSBM event
  !------------------------------------------------------
  nNSB_tot=0
  do icpu=1,ncpu
  ! Loop over cpus
     igrid=headl(icpu,levelmin)
     ! Loop over grids
     do jgrid=1,numbl(icpu,levelmin)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0        
        ! Count old enough star particles that have not exploded
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)

              if( idp(ipart).gt.0 .and. tp(ipart).ne.0d0 .and. tmp(ipart) > 0.0 .and. tmp(ipart) < 10000.0 .and. & ! RS - idp > 0 & tmp > 0.0 means its a NSB remnant
                   & tp(ipart).lt.(t-tmp(ipart) ) ) then ! RS - sim times counts up toward 0 from neg. Ensure we're past tmp
!                 print *, "check_for_nsb: Found a particle that is past merge time: time merge, delay", t-tmp(ipart), tmp(ipart)
                 npart2=npart2+1
              endif
              ipart=next_part  ! Go to next particle
           end do
        endif
        
        nNSB_tot=nNSB_tot+npart2   ! Add NSB to the total
        igrid=next(igrid)   ! Go to next grid
     end do
  enddo

  !
  ! Sync counts across processors
  !
  nNSB_icpu=0
  nNSB_icpu(myid)=nNSB_tot  ! RS - Total number of star particles eligible for NSB from this CPU
#ifndef WITHOUTMPI
  ! Give an array of number of NSB on each cpu available to all cpus
  call MPI_ALLREDUCE(nNSB_icpu,nNSB_icpu_all,ncpu,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
  nNSB_icpu=nNSB_icpu_all ! Update the array of NSB particles from each CPU
#endif
  nNSB_tot=sum(nNSB_icpu(1:ncpu)) ! RS - Total number of SP eligible for NSB across ALL CPUs

  if(myid==1)then
     write(*,*)'-----------------------------------------------'
     write(*,*)'Number of NSB to explode=', nNSB_tot
     write(*,*)'-----------------------------------------------'
  endif

  ! If there are no NSBs, just return
  if (nNSB_tot .eq. 0) return

  ! Allocate arrays for the position and the mass of the NSB
  ! RS- Adding rp support
  allocate(xNSB_tot(1:nNSB_tot,1:3),vNSB_tot(1:nNSB_tot,1:3),mNSB_tot(1:nNSB_tot),itemp(1:nNSB_tot),rpNSB_tot(1:nNSB_tot))

  xNSB_tot=0.;vNSB_tot=0.;mNSB_tot=0.;rpNSB_tot=0. ! MTS, RS - initial arrays to 0

  !------------------------------------------------------
  ! Give position and mass of the star to the NSB array
  !------------------------------------------------------
  if(myid==1)then
     iNSB=0
  else
     iNSB=sum(nNSB_icpu(1:myid-1)) ! total across CPUs
  endif
  do icpu=1,ncpu
  ! Loop over cpus
     igrid=headl(icpu,levelmin)
     ! Loop over grids
     do jgrid=1,numbl(icpu,levelmin)
        npart1=numbp(igrid)  ! Number of particles in the grid
        ! Count old enough star particles that have not exploded
        if(npart1>0)then
           ipart=headp(igrid) ! RS - pointer to head of particle list
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart) ! Pointer to particle right after ipart
              if( idp(ipart).gt.0 .and. tp(ipart).ne.0d0 .and. tmp(ipart) > 0.0 .and. tmp(ipart) < 10000.0 .and. &  ! Only tmp > 0.0 particles are set to merge. 
                   & tp(ipart).lt.(t-tmp(ipart) )) then
                 iNSB=iNSB+1
                 xNSB_tot(iNSB,1)=xp(ipart,1)
                 xNSB_tot(iNSB,2)=xp(ipart,2)
                 xNSB_tot(iNSB,3)=xp(ipart,3)
                 vNSB_tot(iNSB,1)=vp(ipart,1)
                 vNSB_tot(iNSB,2)=vp(ipart,2)
                 vNSB_tot(iNSB,3)=vp(ipart,3)
                 mNSB_tot(iNSB)  = 0.001*2d33/(scale_d*scale_l**3)       ! MTS Ejecta mass in NSM event 
		 rpNSB_tot(iNSB) = r_abundance(ipart)
                 ! Remove the mass ejected by the NSB
                 mp(ipart) = mp(ipart)-mNSB_tot(iNSB)
                 tmp(ipart) = 0.0 ! RS - this remnant is going to merge ... set to 0.0 so we don't look at it again.
                print *, "MTS,NSM ID,x,y,z ",idp(ipart),xp(ipart,1),xp(ipart,2),xp(ipart,3)
              endif
              ipart=next_part  ! Go to next particle
           end do
        endif
     
        igrid=next(igrid)   ! Go to next grid
     end do
  end do 
  ! End loop over levels

  ! Sync across CPUs and allocate arrays
#ifndef WITHOUTMPI
  allocate(xNSB_all(1:nNSB_tot,1:3),vNSB_all(1:nNSB_tot,1:3),mNSB_all(1:nNSB_tot), rpNSB_all(1:nNSB_tot)) 
  call MPI_ALLREDUCE(xNSB_tot,xNSB_all,nNSB_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(vNSB_tot,vNSB_all,nNSB_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(mNSB_tot,mNSB_all,nNSB_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(rpNSB_tot,rpNSB_all,nNSB_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info) ! rp MTS

  ! Copy back sums into local 'tot'
  xNSB_tot=xNSB_all
  vNSB_tot=vNSB_all
  mNSB_tot=mNSB_all
  rpNSB_tot=rpNSB_all
  deallocate(xNSB_all,vNSB_all,mNSB_all,rpNSB_all) ! Copied in the rpNSB_all into rpNSB_tot above, no longer needed, MTS
#endif

  ! ****************************************************************************
  ! ****************************************************************************
  ! RS - POTENTIAL BUG?? CHECK
  ! I THINK we can reuse this routine with the NSBs
  call getSNonmyid(itemp,nNSB,xNSB_tot,nNSB_tot)
  ! ****************************************************************************
  ! ****************************************************************************

  ! Allocate the arrays for the position and the mass of the NSB
  allocate(xNSB(1:nNSB,1:3),vNSB(1:nNSB,1:3),mNSB(1:nNSB),iNSB_myid(1:nNSB), rpNSB(1:nNSB)) ! RS - Adding PSN, MTS
  xNSB=0d0; vNSB=0d0; mNSB=0d0; iNSB_myid=0

  do iNSB=1,nNSB
     isort=itemp(iNSB)
     iNSB_myid(iNSB)=isort
     xNSB(iNSB,1)=xNSB_tot(isort,1)
     xNSB(iNSB,2)=xNSB_tot(isort,2)
     xNSB(iNSB,3)=xNSB_tot(isort,3)
     vNSB(iNSB,1)=vNSB_tot(isort,1)
     vNSB(iNSB,2)=vNSB_tot(isort,2)
     vNSB(iNSB,3)=vNSB_tot(isort,3)
     mNSB(iNSB)  =mNSB_tot(isort)
     rpNSB(iNSB) =rpNSB_tot(isort)! MTS
  enddo
  deallocate(xNSB_tot,vNSB_tot,mNSB_tot,rpNSB_tot, itemp) ! MTS

  allocate(vol_gas(1:nNSB),dq(1:nNSB,1:3),ekBlast(1:nNSB),indNSB(1:nNSB))
  allocate(mloadNSB(1:nNSB),vloadNSB(1:nNSB,1:3), rploadNSB(1:nNSB)) ! MTS

  ! Compute the grid discretization effects
  call average_NSB(xNSB,vNSB,vol_gas,dq,ekBlast,indNSB,nNSB,nNSB_tot,iNSB_myid,mNSB,mloadNSB, rpNSB, rploadNSB,vloadNSB) ! MTS
  
  deallocate(rpNSB) ! MTS
  ! Modify hydro quantities to account for a Sedov blast wave
  call Sedov_blast_NSB(xNSB,mNSB,indNSB,vol_gas,dq,ekBlast,nNSB,mloadNSB,vloadNSB, rploadNSB) 

  deallocate(xNSB,vNSB,mNSB,iNSB_myid)
  deallocate(indNSB,vol_gas,dq,ekBlast)
  deallocate(mloadNSB,vloadNSB, rploadNSB) ! Release Z3loadSN, MTS

end subroutine check_for_NSB

!################################################################
!################################################################
! Random draw for delay to NSB merge 
! R. Sarmento, M. Safarzadeh 13 July 2017
! Return a time (delay) to merge in Myr from a power law distribution (n=-1)
! with mean time = 180 Myr
!################################################################
!################################################################
subroutine merge_time(t_merge)
  use pm_commons
!  real(dp), intent(out):: t_merge
  real(dp):: t_merge
  real(kind=8)::RandNum
  real(dp):: t_min = 1.0, t_max = 10.0 !!! REAL SHORT... need to put back to 10000 - RS
  
  call ranf(localseed,RandNum)
  t_merge = t_min * (t_max/t_min)**RandNum
  t_merge=5.
  print *,"merge_time: merge time: ",t_merge
end subroutine merge_time

!################################################################
!################################################################
! Random draw for natal kick velocity components
! R. Sarmento, M. Safarzadeh 13 July 2017
! Return 3 components of a random velocity pulled from an exp
! distribution. Note to get isotropy we need to pull theta from
! P(th) = sin(th)... 
!################################################################
!################################################################
subroutine natal_kick(dvx,dvy,dvz)
  use pm_commons
  real(dp):: dvx, dvy, dvz
  
  !
  ! Return a random velocity according to a declining exp(-v/v_mean)
  !
  real(kind=8)::RandNum
  real(dp):: vv, yy, v_mean_km_s=180.0, pi=3.1415
  
  call ranf(localseed,RandNum)
  if (RandNum > 0.999) RandNum = 0.999 ! Avoid inf at tail of exp distribution, 0.999 -> ~1200 km/s
  vv = -v_mean_km_s * LOG(1-RandNum)
  vv = 400.0 ! Use fixed natal kick in this version

  print *, "natal_kick: mag vv = ",vv

  ! To generate an isotropic distribution on the sphere, we need to 
  ! choose theta from P(t) = sin(t) ... results in acos(1-2*RandNum) 
  call ranf(localseed,RandNum)     
  phi = (2.0 * pi * RandNum) - pi ! Random angle in x-y plane: -180->180, flat distro
  call ranf(localseed,RandNum)      
  theta = pi - ACOS(1.0-2.0 * RandNum) ! Distro in y-z plane favors equator... 
  
  dvx = vv * COS(phi) * COS(theta)
  dvy = vv * SIN(phi) * COS(theta)
  dvz = vv * SIN(theta)
  print *, "natal_kick: phi, theta", phi, theta
  print *, "natal_kick: vx, vy, vz", dvx, dvy, dvz
  
end subroutine natal_kick

!################################################################
!################################################################
! Called before thermal_feedback (above) in amr_step...
!################################################################
!################################################################
subroutine kinetic_feedback
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
  integer::nSN_tot_all
  real(kind=8)::RandNum  ! RS-MTS angles for the random natal kick
  integer,dimension(1:ncpu)::nSN_icpu_all,nNSB_icpu_all ! MTS/RS - count of NSBs
  real(dp),dimension(:),allocatable::mSN_all,ZSN_all,PSN_all,rpSN_all ! RS - arrays for particle pristine frac
  real(dp),dimension(:,:),allocatable::xSN_all,vSN_all
#endif
  !----------------------------------------------------------------------
  ! Description: This subroutine checks SN events in cells where a
  ! star particle has been spawned.
  ! Yohan Dubois
  !----------------------------------------------------------------------
  ! local constants
  integer::icpu,igrid,jgrid,npart1,npart2,ipart,jpart,next_part
  integer::nSN,nSN_tot,info,iSN,ilevel,ivar,iNSB
  integer::nNSB ! RS-MTS
  integer,dimension(1:ncpu)::nSN_icpu,nNSB_icpu
  logical ::ok_free
!#ifdef SOLVERhydro
!  integer ::imetal=6
!#endif
!#ifdef SOLVERmhd
!  integer ::imetal=9
!#endif
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v,t0,mass_load, t_merge, tMyr, dvx, dvy, dvz
  integer ,dimension(:),allocatable::indSN,iSN_myid
  ! Rick Sarmento - Adding PSN array tracking the part pristine fraction and Z3loadSN for gas xfer
  real(dp),dimension(:),allocatable::mSN,ZSN,PSN,vol_gas,ekBlast,mloadSN,ZloadSN, Z3loadSN,mloadNSB
  real(dp),dimension(:,:),allocatable::xSN,vSN,dq,vloadSN
  integer ,dimension(:),allocatable::indSN_tot,itemp
  real(dp),dimension(:),allocatable::mSN_tot,ZSN_tot,PSN_tot ! RS - arrays for particle prist frac 
  real(dp),dimension(:,:),allocatable::xSN_tot,vSN_tot
  real(dp),dimension(:),allocatable:: rpSN,rploadSN,rpSN_tot ! MTS rpgas
  integer::isort

  if(.not. hydro)return
  if(ndim.ne.3)return

  if(verbose)write(*,*)'Entering kinetic_feedback'

  !----------------------------------------------------------------------
  ! RS
  ! Check to see if a previous set of SN have reached their merger time
  ! If so, explode them and update rp_gas
  !----------------------------------------------------------------------
  call check_for_NSB()
  !----------------------------------------------------------------------
  
  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Time delay for SN explosion from Myr to code units
  t0=t_delay*(1d6*365.*24.*3600.)/scale_t ! RS - t_delay is 10 Myr in amr_parameters.f90
  tMyr=1.0*(1d6*365.*24.*3600.)/scale_t ! RS - This is 1 Myr

  !------------------------------------------------------
  ! Gather star particles eligible for a SN event
  !------------------------------------------------------
  nSN_tot=0
  do icpu=1,ncpu
  ! Loop over cpus
     igrid=headl(icpu,levelmin)
     ! Loop over grids
     do jgrid=1,numbl(icpu,levelmin)
        npart1=numbp(igrid)  ! Number of particles in the grid
        npart2=0        
        ! Count old enough star particles that have not exploded
        if(npart1>0)then
           ipart=headp(igrid)
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart)
              if( idp(ipart).lt.0 .and. tp(ipart).ne.0d0 .and. & ! RS - idp < 0 means its a star particle
                   & tp(ipart).lt.(t-t0) )then ! RS - sim times counts up toward 0 from neg. Ensure we're t_delay past
                 npart2=npart2+1
              endif
              ipart=next_part  ! Go to next particle
           end do
        endif
        
        nSN_tot=nSN_tot+npart2   ! Add SNe to the total
        igrid=next(igrid)   ! Go to next grid
     end do
  enddo

  nSN_icpu=0
  nSN_icpu(myid)=nSN_tot  ! RS - Total number of star particles eligible for SN from this CPU
#ifndef WITHOUTMPI
  ! Give an array of number of SN on each cpu available to all cpus
  call MPI_ALLREDUCE(nSN_icpu,nSN_icpu_all,ncpu,MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,info)
  nSN_icpu=nSN_icpu_all ! Update the array of SN particles from each CPU
#endif
  nSN_tot=sum(nSN_icpu(1:ncpu)) ! RS - Total number of SP eligible for SN across ALL CPUs

  if(myid==1)then
     write(*,*)'-----------------------------------------------'
     write(*,*)'Number of SN to explode=',nSN_tot
     write(*,*)'-----------------------------------------------'
  endif

  if (nSN_tot .eq. 0) return
  
  ! Allocate arrays for the position and the mass of the SN
  ! Rick Sarmento - Adding PSN_tot to track the pristine fraction of the particle
  allocate(xSN_tot(1:nSN_tot,1:3),vSN_tot(1:nSN_tot,1:3),mSN_tot(1:nSN_tot),ZSN_tot(1:nSN_tot),PSN_tot(1:nSN_tot),itemp(1:nSN_tot))

  xSN_tot=0.;vSN_tot=0.;mSN_tot=0.;rpSN_tot=0.;ZSN_tot=0.0;PSN_tot=0.0;

  !------------------------------------------------------
  ! Give position and mass of the star to the SN array
  !------------------------------------------------------
  if(myid==1)then
     iSN=0
  else
     iSN=sum(nSN_icpu(1:myid-1))
  endif
  do icpu=1,ncpu
     ! Loop over cpus
     igrid=headl(icpu,levelmin)
     ! Loop over grids
     do jgrid=1,numbl(icpu,levelmin)
        npart1=numbp(igrid)  ! Number of particles in the grid
        ! Count old enough star particles that have not exploded
        if(npart1>0)then
           ipart=headp(igrid) ! RS - pointer to head of particle list
           ! Loop over particles
           do jpart=1,npart1
              ! Save next particle   <--- Very important !!!
              next_part=nextp(ipart) ! Pointer to particle right after ipart
              !
              ! First, check to see if a SP has been tagged (during a prev time-step) to be a NSB
              ! If so, compute a merger time.
              !
              if( tp(ipart).ne.0d0 .and. tmp(ipart) == 10000.0) then ! SP was selected in the prev time-step to be a NSB
                 ! Compute the merger time for the NSBM
                 call merge_time(t_merge)
                 tmp(ipart) = t_merge * (1d6*365.*24.*3600.)/scale_t 
                 print *,"kinetic_feedback: SP designated for SN and for NSB with delay (Myr) ", t_merge
!                 print *,"kinetic_feedback: SP designated for SN and for NSB with delay (code units) ", tmp(ipart)
              end if
!              call ranf(localseed,RandNum)
!              if( idp(ipart).lt.0 .and. tp(ipart).ne.0d0 .and. & ! ID < 0 & birthTime != 0 (not a DM particle)
!                   & tp(ipart).lt.(t-t0) .and. (RandNum < 0.5) .and. (tp(ipart).gt.(t-t0-tMyr)))then ! & t0 Myr have passed since birthTime (tp). This SP can go SN
              if( idp(ipart).lt.0 .and. tp(ipart).ne.0d0 .and. & ! ID < 0 & birthTime != 0 (not a DM particle)
                   & tp(ipart).lt.(t-t0) )then ! Normal conditions.
                 iSN=iSN+1
                 xSN_tot(iSN,1)=xp(ipart,1)
                 xSN_tot(iSN,2)=xp(ipart,2)
                 xSN_tot(iSN,3)=xp(ipart,3)
                 vSN_tot(iSN,1)=vp(ipart,1)
                 vSN_tot(iSN,2)=vp(ipart,2)
                 vSN_tot(iSN,3)=vp(ipart,3)
                 if (metal) ZSN_tot(iSN)=zp(ipart)
                 ! Rick Sarmento -
                 ! We need to keep track of the particles' pristine fraction
                 ! When we do mass loading we'll have to adjust the primordial Z of the
                 ! star particle.
                 if (prist_gas_fraction) PSN_tot(iSN)=pfp(ipart) ! pfp: sp's pristing frac
                 
                 ! **********************************************************
                 ! RS - 20 July 2016
                 ! Change mass yield based on pristine fraction of the sp.
                 ! For Pop III use eta_sn3 as the mass fraction returned to 
                 ! the ism.
                 ! Right now eta_sn3 = eta_sn so we have one type of SN
                 ! **********************************************************
                 ! Fixing the ejecta mass at 10 M_sun since we know our SP mass is 50 M_sun for our ZOOM sim.
                 mSN_tot(iSN) = 10.0 * 2d33/(scale_d*scale_l**3)
                 if (mSN_tot(iSN) > mp(ipart)) print *,"feedbk: *** ERROR - mass loss in mSN > mp *** ", mSN_tot(iSN), mp(ipart)
!!                 print *,"feedbk: orig mass loss, mass loss 10 M_sun, particle mass ", eta_sn*mp(ipart), mSN_tot(iSN), mp(ipart)
!!$                 mSN_tot(iSN)  =eta_sn*mp(ipart)       ! Orig code... 
                 ! Remove the mass ejected by the SN
                 mp(ipart) = mp(ipart)-mSN_tot(iSN)
!!$                 mp(ipart) =mp(ipart)-eta_sn*mp(ipart) ! Orig code... 
                 ! Also compute energy, its dependent on SN type: 
                 ! NO... assume energy per unit mass in IMF is the same... (per discussion with Evan)
!!$                 ESN_tot(iSN) = ((1d51/(10d0*2d33))/scale_v**2)*(1.0-PSN_tot(iSN)) + & ! Regular SN
!!$                              &  10d0*(1d51/(10d0*2d33))/scale_v**2 * PSN_tot(iSN)   ! PISN SN, 10x Reg SN Energy
!!$                 print *,"old, new sn ejecta E",((1d51/(10d0*2d33))/scale_v**2),ESN_tot(iSN)
                 idp(ipart) =-idp(ipart) ! Negate the id, this particle has gone SN
                 
                 ! **********************************************************
                 ! RS- determine if this SN should also be a NSB
                 ! **********************************************************
                 call ranf(localseed,RandNum)
                 ! Using 1:100 for now -- testing
!                 print *,"kinetic_feedback: RANDOM NUMBER : ", RandNum
!                 print *,"kinetic_feedback: tmp : ", tmp(ipart) 
                 if (RandNum < 0.01 .and. tmp(ipart) == -1.0) then ! Pick 1:1000 for NSB, -1.0 means this SP hasn't gone NSBM
                    print *,"kinetic_feedback: selected particle for NSBM"
                    tmp(ipart) = 10000.0 ! Tag value for NSBM. tmp is delay in Myr, but 10000.0 is higher than selection range [20,10000] in code units: A flag.
                    idp(ipart) =-idp(ipart) ! This SN is now part of a binary system. Allow it to go SN again... at next step!
                    ! Note that the particle is available for SN (because of idp < 0.0 again) but it cannot get in here again
                    ! since tmp = 1.0 now. Ensures we just goes SN twice, but that we don't get caught in a inf loop.
!                    print *,"kinetic_feedback: particle ID should be neg again: ", idp(ipart)
                    ! Apply a natal kick to the SP
                    ! Note that the particle's vel as already been copied out to vSN_tot so this won't effect the
                    ! ejecta energy, only the post-SN particle velocity
                    print *,"MTS,original vel (x,y,z km/s)",vp(ipart,1)*scale_v/1e5,vp(ipart,2)*scale_v/1e5,vp(ipart,3)*scale_v/1e5
                    call natal_kick(dvx,dvy,dvz) ! Return values in km/s
                    vp(ipart,1) = vp(ipart,1) + dvx * 1e5/scale_v
                    vp(ipart,2) = vp(ipart,2) + dvy * 1e5/scale_v
                    vp(ipart,3) = vp(ipart,3) + dvz * 1e5/scale_v
                    print *,"MTS,Natal kick vel (x,y,z km/s) ",dvx, dvy, dvz
                    print *,"MTS,new vel (x,y,z km/s)",vp(ipart,1)*scale_v/1e5,vp(ipart,2)*scale_v/1e5,vp(ipart,3)*scale_v/1e5
                    print *, "MTS,Binary ID,x,y,z",idp(ipart),xp(ipart,1),xp(ipart,2),xp(ipart,3)
!                    print *,"Natal kick vel (x,y,z internal) ",vp(ipart,1),vp(ipart,2),vp(ipart,3)
                 else if (tmp(ipart) == -1.0) then
!                    print *,"kinetic_feedback: particle not selected .. setting to 0"
                    tmp(ipart) = 0.0 ! Particle not-selected for NSB. Don't reconsider these.
                 endif
              endif
              ipart=next_part  ! Go to next particle
           end do
        endif
     
        igrid=next(igrid)   ! Go to next grid
     end do
  end do 
  ! End loop over levels

#ifndef WITHOUTMPI
  allocate(xSN_all(1:nSN_tot,1:3),vSN_all(1:nSN_tot,1:3),mSN_all(1:nSN_tot),ZSN_all(1:nSN_tot),PSN_all(1:nSN_tot)) 
  call MPI_ALLREDUCE(xSN_tot,xSN_all,nSN_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(vSN_tot,vSN_all,nSN_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(mSN_tot,mSN_all,nSN_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(ZSN_tot,ZSN_all,nSN_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(PSN_tot,PSN_all,nSN_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info) ! Particle pristine fraction
!!$  call MPI_ALLREDUCE(ESN_tot,ESN_all,nSN_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info) ! Particle SN energy
  ! RS - July 20 2016: added ESN for Sedov_Blast so it can use SN-type specific energy

  ! Copy back sums into local 'tot'
  xSN_tot=xSN_all
  vSN_tot=vSN_all
  mSN_tot=mSN_all
  ZSN_tot=ZSN_all
  PSN_tot=PSN_all
!!$  ESN_tot=ESN_all 
  deallocate(xSN_all,vSN_all,mSN_all,ZSN_all,PSN_all) 
#endif

  call getSNonmyid(itemp,nSN,xSN_tot,nSN_tot)

  ! Allocate the arrays for the position and the mass of the SN
  allocate(xSN(1:nSN,1:3),vSN(1:nSN,1:3),mSN(1:nSN),ZSN(1:nSN),PSN(1:nSN),iSN_myid(1:nSN)) ! RS - Adding PSN, MTS
  xSN=0d0; vSN=0d0; mSN=0d0; ZSN=0d0; PSN=0d0; iSN_myid=0

  do iSN=1,nSN
     isort=itemp(iSN)
     iSN_myid(iSN)=isort
     xSN(iSN,1)=xSN_tot(isort,1)
     xSN(iSN,2)=xSN_tot(isort,2)
     xSN(iSN,3)=xSN_tot(isort,3)
     vSN(iSN,1)=vSN_tot(isort,1)
     vSN(iSN,2)=vSN_tot(isort,2)
     vSN(iSN,3)=vSN_tot(isort,3)
     mSN(iSN)=mSN_tot(isort)
     ZSN(iSN)=ZSN_tot(isort) ! ZSN tracks the Z of the SNe at index iSN - RS
     PSN(iSN)=PSN_tot(isort) ! Rick Sarmento - Adding an array with the particle's pristine fraction 
  enddo
  deallocate(xSN_tot,vSN_tot,mSN_tot,ZSN_tot,PSN_tot, itemp) ! MTS

  allocate(vol_gas(1:nSN),dq(1:nSN,1:3),ekBlast(1:nSN),indSN(1:nSN))
  allocate(mloadSN(1:nSN),ZloadSN(1:nSN),Z3loadSN(1:nSN),vloadSN(1:nSN,1:3)) ! MTS

  ! Compute the grid discretization effects
  ! Rick Sarmento - PSN is an array of the star particles' pristine fraction. 
  ! Z3loadSN generated by average_SN ... needed in Sedov_blast
  call average_SN(xSN,vSN,vol_gas,dq,ekBlast,indSN,nSN,nSN_tot,iSN_myid,mSN,mloadSN,ZSN,PSN, ZloadSN,Z3loadSN,vloadSN ) ! MTS
  ! The above populates ZloadSN, need to add Z3loadSN
  
  deallocate(PSN) 
  ! Modify hydro quantities to account for a Sedov blast wave
  ! Z3loadSN needed when blast mass xfer
  call Sedov_blast(xSN,mSN,indSN,vol_gas,dq,ekBlast,nSN,mloadSN,ZloadSN,Z3loadSN,vloadSN ) ! RS - Added Z3loadSN 

  deallocate(xSN,vSN,mSN,ZSN,iSN_myid)
  deallocate(indSN,vol_gas,dq,ekBlast)
  deallocate(mloadSN,ZloadSN,Z3loadSN,vloadSN ) ! Release Z3loadSN 

  ! Update hydro quantities for split cells
  do ilevel=nlevelmax,levelmin,-1
     call upload_fine(ilevel)
#ifdef SOLVERmhd
     do ivar=1,nvar+3
#else
     do ivar=1,nvar
#endif
        call make_virtual_fine_dp(uold(1,ivar),ilevel)
     enddo
  enddo

end subroutine kinetic_feedback
!################################################################
!################################################################
!################################################################
!################################################################
subroutine average_NSB(xNSB,vNSB,vol_gas,dq,ekBlast,ind_blast,nNSB,nNSB_tot,iNSB_myid, mNSB,mloadNSB,rpNSB, rploadNSB, vloadNSB) 

  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !------------------------------------------------------------------------
  ! This routine average the hydro quantities inside the NSB bubble
  ! and do the mass loading process
  ! Called by kinetic_feedback
  !------------------------------------------------------------------------
  integer::ilevel,ncache,nNSB,nNSB_tot,j,iNSB,ind,ix,iy,iz,ngrid,iskip
  integer::i,nx_loc,igrid,info
  integer,dimension(1:nvector),save::ind_grid,ind_cell
  real(kind=8)::RandNum
!#ifdef SOLVERhydro
!  integer ::imetal=6
!#endif
!#ifdef SOLVERmhd
!  integer ::imetal=9
!#endif
  real(dp)::x,y,z,dr_NSB,d,u,v,w,ek,u2,v2,w2,dr_cell
  real(dp)::scale,dx,dxx,dyy,dzz,dx_min,dx_loc,vol_loc,rmax2,rmax
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp)::eint,ekk,ekk1,ekk2,heat,mload,cpgf,ctv, rpload,newRP ! MTS
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  integer ,dimension(1:nNSB)::ind_blast
  real(dp),dimension(1:nNSB)::mNSB,mloadNSB, rploadNSB, vol_gas,ekBlast 
  real(dp),dimension(1:nNSB)::rpNSB ! MTS
  real(dp),dimension(1:nNSB,1:3)::xNSB,vNSB,dq,u2Blast,vloadNSB
#ifndef WITHOUTMPI
  real(dp),dimension(1:nNSB_tot)::vol_gas_mpi
  real(dp),dimension(1:nNSB_tot)::vol_gas_all
  real(dp),dimension(1:nNSB_tot)::mloadNSB_mpi,mloadNSB_all ! RS - adding Z3load...
  real(dp),dimension(1:nNSB_tot,1:3)::dq_mpi,u2Blast_mpi,vloadNSB_mpi
  real(dp),dimension(1:nNSB_tot,1:3)::dq_all,u2Blast_all,vloadNSB_all
  real(dp),dimension(1:nNSB_tot)::rploadNSB_all,rploadNSB_mpi ! MTS
#endif
  logical ,dimension(1:nvector),save::ok
  integer ,dimension(1:nNSB)::iNSB_myid
  integer::ind_NSB

  if(verbose)write(*,*)'Entering average_NSB'

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_min=scale*0.5D0**nlevelmax
  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Maximum radius of the ejecta
  rmax=MAX(1.5d0*dx_min*scale_l/aexp,rbubble*3.08d18)
  rmax=rmax/scale_l
  rmax2=rmax*rmax

  ! Initialize the averaged variables
  vol_gas=0.0;dq=0.0;u2Blast=0.0;ekBlast=0.0;ind_blast=-1;mloadNSB=0.0;vloadNSB=0.0

  ! Loop over levels
  do ilevel=levelmin,nlevelmax
     ! Computing local volume (important for averaging hydro quantities) 
     dx=0.5D0**ilevel     ! ilevel 1->0.5, 2->0.25 ... dx is fractional box len
     dx_loc=dx*scale      ! Fraction of boxlen (which starts at 1 for ilevel 0)
     vol_loc=dx_loc**ndim ! So this is a volume as a fraction of total boxlen units
     ! Cells center position relative to grid center position
     do ind=1,twotondim  
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
     end do

     ! Loop over grids
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do

        ! Loop over cells
        do ind=1,twotondim  
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do

           ! Flag leaf cells
           do i=1,ngrid
              ok(i)=son(ind_cell(i))==0
           end do

           do i=1,ngrid
              if(ok(i))then
                 ! Get gas cell position
                 ! xc is cell center, xg grids position
                 x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                 y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                 z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
                 do iNSB=1,nNSB
                    ! Distance from cell center to NSB: 
                    dxx=x-xNSB(iNSB,1)
                    dyy=y-xNSB(iNSB,2)
                    dzz=z-xNSB(iNSB,3)
                    dr_NSB=dxx**2+dyy**2+dzz**2 ! Distance to NSB squared - RS
                    dr_cell=MAX(ABS(dxx),ABS(dyy),ABS(dzz)) ! Largest delta distance dim - RS
                    if(dr_NSB.lt.rmax2)then ! Compare distance^2 to bubble radius^2... - RS
                       vol_gas(iNSB)=vol_gas(iNSB)+vol_loc ! Add up gas for all cells in bubble
                       ! Take account for grid effects on the conservation of the
                       ! normalized linear momentum
                       u=dxx/rmax ! Distance as fraction of max radius of NSB - RS
                       v=dyy/rmax
                       w=dzz/rmax
                       ! Add the local normalized linear momentum to the total linear
                       ! momentum of the blast wave (should be zero with no grid effect)
                       dq(iNSB,1)=dq(iNSB,1)+u*vol_loc
                       dq(iNSB,2)=dq(iNSB,2)+v*vol_loc
                       dq(iNSB,3)=dq(iNSB,3)+w*vol_loc
                       u2Blast(iNSB,1)=u2Blast(iNSB,1)+u*u*vol_loc
                       u2Blast(iNSB,2)=u2Blast(iNSB,2)+v*v*vol_loc
                       u2Blast(iNSB,3)=u2Blast(iNSB,3)+w*w*vol_loc
                    endif
                    if(dr_cell.le.dx_loc/2.0)then ! dx_loc is size of cell
                       ! We get in here if the NSB went off in THIS cell
                       ind_blast(iNSB)=ind_cell(i) ! This is the cell with the blast!
                       ekBlast  (iNSB)=vol_loc     ! Not sure what's going on here??
                       d=uold(ind_blast(iNSB),1)
                       if (d < smallr) print *,"ave_NSB d < smallr but using d:", d, smallr
                       u=uold(ind_blast(iNSB),2)/d
                       v=uold(ind_blast(iNSB),3)/d
                       w=uold(ind_blast(iNSB),4)/d
                       ekk=0.5d0*d*(u*u+v*v+w*w)       ! 0.5 rho v^2 - KE of gas in cell
                       eint=uold(ind_blast(iNSB),5)-ekk ! Internal gas energy... 
                       ! Mass loading factor of the Sedov explosion
                       ! Ensure that no more that 25% of the gas content is removed
                       mload=min(f_w*mNSB(iNSB),0.25d0*d*vol_loc) ! f_w is mass loading factor for gas from cell
                       ! At this point mload is either 25% of the cells mass or f_w * mNSB
                       mloadNSB(iNSB)=mNSB(iNSB)+mload ! mloadNSB is the mass of the NSB ejecta AND the gas carried along...
                       ! Should mloadNSB be used to degrade the PGF immediately? Right here? Talk to Evan... 
                       
                       if (rpgas) then !MTS. 
                          rpload=uold(ind_blast(iNSB),irpgas)/d ! MTS current gas r-p
                          !! newRP = yield_rp*(1.0-rpNSB(iNSB))*mNSB(iNSB)! yield_rp = 0 MTS 
                          rploadNSB(iNSB) = ( mload*rpload + 1*mNSB(iNSB) ) / mloadNSB(iNSB)!MTS 

                          if (mloadNSB(iNSB) <= 0.0) print *,"FEEDBACK ERR: mloadNSB(iNSB) :", mloadNSB(iNSB) ! DEBUG -- RS
                          ! print *, "average_NSB: mload, mNSB(iNSB), rpload", mload, mNSB(iNSB), rpload
                          uold(ind_blast(iNSB),irpgas)=uold(ind_blast(iNSB),irpgas)-(rpload*mload/vol_loc)
!                          print *, "average_NSB: (rpload*mload/vol_loc) leaving the cell ", (rpload*mload/vol_loc)
!                          if (uold(ind_blast(iNSB),irpgas) < 0.0 ) then
!                             print *, "average_NSB ERROR: irpgas < 0.0", uold(ind_blast(iNSB),irpgas) ! DEBUG - RS
!                             print *, "average_NSB ERROR: irpgas lost this much (add to above to get orig):",rpload*mload/vol_loc
!                          end if
                          ! print *, "average_NSB: rpgas ", uold(ind_blast(iNSB),irpgas)/uold(ind_blast(iNSB),1)
                       end if

                       d=uold(ind_blast(iNSB),1)-mload/vol_loc ! Very important! Updates the cell density - RS

                       uold(ind_blast(iNSB),1)=d
                       uold(ind_blast(iNSB),2)=d*u
                       uold(ind_blast(iNSB),3)=d*v
                       uold(ind_blast(iNSB),4)=d*w
                       uold(ind_blast(iNSB),5)=eint+0.5d0*d*(u*u+v*v+w*w)

                       vloadNSB(iNSB,1)=(mNSB(iNSB)*vNSB(iNSB,1)+mload*u)/mloadNSB(iNSB)
                       vloadNSB(iNSB,2)=(mNSB(iNSB)*vNSB(iNSB,2)+mload*v)/mloadNSB(iNSB)
                       vloadNSB(iNSB,3)=(mNSB(iNSB)*vNSB(iNSB,3)+mload*w)/mloadNSB(iNSB)
                    endif
                 end do
              endif
           end do
           
        end do
        ! End loop over cells
     end do
     ! End loop over grids
  end do
  ! End loop over levels

  !################################################################
#ifndef WITHOUTMPI
  vol_gas_mpi=0d0; dq_mpi=0d0; u2Blast_mpi=0d0; mloadNSB_mpi=0d0; vloadNSB_mpi=0d0
  ! Put the nNSB size arrays into nNSB_tot size arrays to synchronize processors
  do iNSB=1,nNSB
     ind_NSB=iNSB_myid(iNSB)
     vol_gas_mpi(ind_NSB)=vol_gas(iNSB)
     mloadNSB_mpi(ind_NSB)=mloadNSB(iNSB)
     rploadNSB_mpi(ind_NSB)=rploadNSB(iNSB) ! MTS
     vloadNSB_mpi(ind_NSB,1)=vloadNSB(iNSB,1)
     vloadNSB_mpi(ind_NSB,2)=vloadNSB(iNSB,2)
     vloadNSB_mpi(ind_NSB,3)=vloadNSB(iNSB,3)
     dq_mpi     (ind_NSB,1)=dq     (iNSB,1)
     dq_mpi     (ind_NSB,2)=dq     (iNSB,2)
     dq_mpi     (ind_NSB,3)=dq     (iNSB,3)
     u2Blast_mpi(ind_NSB,1)=u2Blast(iNSB,1)
     u2Blast_mpi(ind_NSB,2)=u2Blast(iNSB,2)
     u2Blast_mpi(ind_NSB,3)=u2Blast(iNSB,3)
  enddo
  call MPI_ALLREDUCE(vol_gas_mpi,vol_gas_all,nNSB_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(mloadNSB_mpi,mloadNSB_all,nNSB_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(rploadNSB_mpi,rploadNSB_all,nNSB_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info) ! MTS
  call MPI_ALLREDUCE(vloadNSB_mpi,vloadNSB_all,nNSB_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(dq_mpi     ,dq_all     ,nNSB_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(u2Blast_mpi,u2Blast_all,nNSB_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  vol_gas_mpi=vol_gas_all
  mloadNSB_mpi=mloadNSB_all
  rploadNSB_mpi=rploadNSB_all ! MTS
  vloadNSB_mpi=vloadNSB_all
  dq_mpi     =dq_all
  u2Blast_mpi=u2Blast_all
  ! Put the nNSB_tot size arrays into nNSB size arrays
  do iNSB=1,nNSB
     ind_NSB=iNSB_myid(iNSB)
     vol_gas(iNSB)=vol_gas_mpi(ind_NSB)
     mloadNSB(iNSB)=mloadNSB_mpi(ind_NSB)
     rploadNSB(iNSB)=rploadNSB_mpi(ind_NSB) ! - MTS
     vloadNSB(iNSB,1)=vloadNSB_mpi(ind_NSB,1)
     vloadNSB(iNSB,2)=vloadNSB_mpi(ind_NSB,2)
     vloadNSB(iNSB,3)=vloadNSB_mpi(ind_NSB,3)
     dq     (iNSB,1)=dq_mpi     (ind_NSB,1)
     dq     (iNSB,2)=dq_mpi     (ind_NSB,2)
     dq     (iNSB,3)=dq_mpi     (ind_NSB,3)
     u2Blast(iNSB,1)=u2Blast_mpi(ind_NSB,1)
     u2Blast(iNSB,2)=u2Blast_mpi(ind_NSB,2)
     u2Blast(iNSB,3)=u2Blast_mpi(ind_NSB,3)
  enddo
#endif
  !################################################################
  ! At this point ekBlast is vol_loc
  do iNSB=1,nNSB
     if(vol_gas(iNSB)>0d0)then
        dq(iNSB,1)=dq(iNSB,1)/vol_gas(iNSB)
        dq(iNSB,2)=dq(iNSB,2)/vol_gas(iNSB)
        dq(iNSB,3)=dq(iNSB,3)/vol_gas(iNSB)
        u2Blast(iNSB,1)=u2Blast(iNSB,1)/vol_gas(iNSB)
        u2Blast(iNSB,2)=u2Blast(iNSB,2)/vol_gas(iNSB)
        u2Blast(iNSB,3)=u2Blast(iNSB,3)/vol_gas(iNSB)
        u2=u2Blast(iNSB,1)-dq(iNSB,1)**2
        v2=u2Blast(iNSB,2)-dq(iNSB,2)**2
        w2=u2Blast(iNSB,3)-dq(iNSB,3)**2
        ekBlast(iNSB)=max(0.5d0*(u2+v2+w2),0.0d0)
        if (ekBlast(iNSB).le.1d-5) ekBlast(iNSB)=0d0
     endif
  end do

  if(verbose)write(*,*)'Exiting average_NSB'

end subroutine average_NSB


! ================================starting subroutine AVERAGE SN===================================
!################################################################
!################################################################
!################################################################
!################################################################
subroutine average_SN(xSN,vSN,vol_gas,dq,ekBlast,ind_blast,nSN,nSN_tot,iSN_myid, mSN,mloadSN,ZSN,PSN, ZloadSN,Z3loadSN,vloadSN) ! MTS
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !------------------------------------------------------------------------
  ! This routine average the hydro quantities inside the SN bubble
  ! and do the mass loading process
  ! Called by kinetic_feedback
  !------------------------------------------------------------------------
  integer::ilevel,ncache,nSN,nSN_tot,j,iSN,ind,ix,iy,iz,ngrid,iskip
  integer::i,nx_loc,igrid,info
  integer,dimension(1:nvector),save::ind_grid,ind_cell
  real(kind=8)::RandNum
!#ifdef SOLVERhydro
!  integer ::imetal=6
!#endif
!#ifdef SOLVERmhd
!  integer ::imetal=9
!#endif
  real(dp)::x,y,z,dr_SN,d,u,v,w,ek,u2,v2,w2,dr_cell
  real(dp)::scale,dx,dxx,dyy,dzz,dx_min,dx_loc,vol_loc,rmax2,rmax
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp)::eint,ekk,ekk1,ekk2,heat,mload,Zload,cpgf,ctv,Z3load
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  integer ,dimension(1:nSN)::ind_blast
  ! RS - Adding PSN: the particles' pristine fraction, Z3loadSN for primordially generated Z gas xfer
  real(dp),dimension(1:nSN)::mSN,mloadSN,ZSN, PSN, ZloadSN, Z3loadSN,vol_gas,ekBlast 
  real(dp),dimension(1:nSN,1:3)::xSN,vSN,dq,u2Blast,vloadSN
#ifndef WITHOUTMPI
  real(dp),dimension(1:nSN_tot)::vol_gas_mpi
  real(dp),dimension(1:nSN_tot)::vol_gas_all
  real(dp),dimension(1:nSN_tot)::mloadSN_mpi,mloadSN_all,ZloadSN_mpi,ZloadSN_all,Z3loadSN_mpi,Z3loadSN_all ! RS - adding Z3load...
  real(dp),dimension(1:nSN_tot,1:3)::dq_mpi,u2Blast_mpi,vloadSN_mpi
  real(dp),dimension(1:nSN_tot,1:3)::dq_all,u2Blast_all,vloadSN_all
#endif
  logical ,dimension(1:nvector),save::ok
  integer ,dimension(1:nSN)::iSN_myid
  integer::ind_SN

  if(verbose)write(*,*)'Entering average_SN'

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_min=scale*0.5D0**nlevelmax
  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Maximum radius of the ejecta
  rmax=MAX(1.5d0*dx_min*scale_l/aexp,rbubble*3.08d18)
  rmax=rmax/scale_l
  rmax2=rmax*rmax

  ! Initialize the averaged variables
  vol_gas=0.0;dq=0.0;u2Blast=0.0;ekBlast=0.0;ind_blast=-1;mloadSN=0.0;ZloadSN=0.0;Z3loadSN=0.0;vloadSN=0.0

  ! Loop over levels
  do ilevel=levelmin,nlevelmax
     ! Computing local volume (important for averaging hydro quantities) 
     dx=0.5D0**ilevel     ! ilevel 1->0.5, 2->0.25 ... dx is fractional box len
     dx_loc=dx*scale      ! Fraction of boxlen (which starts at 1 for ilevel 0)
     vol_loc=dx_loc**ndim ! So this is a volume as a fraction of total boxlen units
     ! Cells center position relative to grid center position
     do ind=1,twotondim  
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
     end do

     ! Loop over grids
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do

        ! Loop over cells
        do ind=1,twotondim  
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do

           ! Flag leaf cells
           do i=1,ngrid
              ok(i)=son(ind_cell(i))==0
           end do

           do i=1,ngrid
              if(ok(i))then
                 ! Get gas cell position
                 ! xc is cell center, xg grids position
                 x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                 y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                 z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
                 do iSN=1,nSN
                    ! Distance from cell center to SN: 
                    dxx=x-xSN(iSN,1)
                    dyy=y-xSN(iSN,2)
                    dzz=z-xSN(iSN,3)
                    dr_SN=dxx**2+dyy**2+dzz**2 ! Distance to SN squared - RS
                    dr_cell=MAX(ABS(dxx),ABS(dyy),ABS(dzz)) ! Largest delta distance dim - RS
                    if(dr_SN.lt.rmax2)then ! Compare distance^2 to bubble radius^2... - RS
                       vol_gas(iSN)=vol_gas(iSN)+vol_loc ! Add up gas for all cells in bubble
                       ! Take account for grid effects on the conservation of the
                       ! normalized linear momentum
                       u=dxx/rmax ! Distance as fraction of max radius of SN - RS
                       v=dyy/rmax
                       w=dzz/rmax
                       ! Add the local normalized linear momentum to the total linear
                       ! momentum of the blast wave (should be zero with no grid effect)
                       dq(iSN,1)=dq(iSN,1)+u*vol_loc
                       dq(iSN,2)=dq(iSN,2)+v*vol_loc
                       dq(iSN,3)=dq(iSN,3)+w*vol_loc
                       u2Blast(iSN,1)=u2Blast(iSN,1)+u*u*vol_loc
                       u2Blast(iSN,2)=u2Blast(iSN,2)+v*v*vol_loc
                       u2Blast(iSN,3)=u2Blast(iSN,3)+w*w*vol_loc
                    endif
                    if(dr_cell.le.dx_loc/2.0)then ! dx_loc is size of cell
                       ! We get in here if the SN went off in THIS cell
                       ind_blast(iSN)=ind_cell(i) ! This is the cell with the blast!
                       ekBlast  (iSN)=vol_loc     ! Not sure what's going on here??
                       d=uold(ind_blast(iSN),1)
                       if (d < smallr) print *,"ave_SN d < smallr but using d:", d, smallr
                       u=uold(ind_blast(iSN),2)/d
                       v=uold(ind_blast(iSN),3)/d
                       w=uold(ind_blast(iSN),4)/d
                       ekk=0.5d0*d*(u*u+v*v+w*w)       ! 0.5 rho v^2 - KE of gas in cell
                       eint=uold(ind_blast(iSN),5)-ekk ! Internal gas energy... 
                       ! Mass loading factor of the Sedov explosion
                       ! Ensure that no more that 25% of the gas content is removed
                       mload=min(f_w*mSN(iSN),0.25d0*d*vol_loc) ! f_w is mass loading factor for gas from cell
                       ! At this point mload is either 25% of the cells mass or f_w * mSN
                       mloadSN(iSN)=mSN(iSN)+mload ! mloadSN is the mass of the SN ejecta AND the gas carried along...
                       ! Should mloadSN be used to degrade the PGF immediately? Right here? Talk to Evan... 
                       ! Update gas mass and metal content in the cell
                       if(metal)then
                          Zload=uold(ind_blast(iSN),imetal)/d ! Zload is blast cell's <Z>
                          ! mload*Zload                   - mass of metals in cell gas carried by SN
                          ! yield*(1.0-ZSN(iSN))*mSN(iSN) - new metals created in the explosion
                          ! ZSN(iSN)*mSN(iSN)             - mass of metals that existed in the star already
                          ZloadSN(iSN) = ( mload*Zload + yield*(1.0-ZSN(iSN))*mSN(iSN) + ZSN(iSN)*mSN(iSN)) / mloadSN(iSN)
                          ! ZloadSN is now the mass FRACTION of the ejecta that is metals
                          uold(ind_blast(iSN),imetal)=uold(ind_blast(iSN),imetal)-Zload*mload/vol_loc
                          ! The above accounts for the metals in ZloadSN LEAVING the cell...
                          ! Rick Sarmento
                          ! However, I'm reducing the primordial Z of the gas based on a mass loading factor
                          ! and blast since some of the primordial metals are going to be carried into
                          ! neghboring cells. 
                          ! Primordial Z is the mass of metals created by POP III stars. 
                          if (prist_gas_fraction) then
                             ! Pristine fraction: iprist - the pristine fraction cell density
                             ! Mass (density) is leaving the cell. We need to adjust the cell's iprist
                             ! density based on the mass of stuff leaving the cell. This ensures the
                             ! pristine fraction for the cell doesn't go UP, but remains the same.
                             cpgf = uold(ind_blast(iSN),iprist)/d ! Recover the pure pristine gas fraction
                             if (cpgf > 1.0d0) cpgf = 1.0d0       ! Handle any numerical rounding issues
!!$                             print *,"ave_SN: cell starting iprist: ", uold(ind_blast(iSN),iprist)
!!$                             print *,"ave_SN: cell starting PGF: ", cpgf
                             uold(ind_blast(iSN),iprist)=cpgf * (d - mload/vol_loc) ! iprist with new cell rho

                             ! Primordial Metals
                             ! Update iprimordZ due to primordial metals lost
                             Z3load=uold(ind_blast(iSN),iprimordz)/d ! Z3load is the cell's primordial Z fraction
                             ! Compute Z3loadSN that we'll use to inject primoridal Z into other cells.
                             ! mload*Z3load            - mass of primordial metals in cell gas carried by SN
                             ! yield*mSN(iSN)*PSN(iSn) - mass of metals created by primordial stars
                             ! Compute the mass of the yield in metals from the pristine stars ONLY
                             Z3loadSN(iSN)=( mload*Z3load + yield*mSN(iSN)*PSN(iSn)) / mloadSN(iSN)

                             ! TEST ONLY
                             ! By making Z3loadSN the same as Z3load we can check the primordial Z
                             ! directly against metallicity (imetal) ... they should track exactly... 
!!$               Z3loadSN(iSN)=( mload*Z3load + yield*(1.0-ZSN(iSN))*mSN(iSN) + ZSN(iSN)*mSN(iSN)) / mloadSN(iSN)

                             ! Remove the mass-density of primordial-Z gas from this cell
                             uold(ind_blast(iSN),iprimordz)=uold(ind_blast(iSN),iprimordz)-(Z3load*mload/vol_loc)
                          endif
                          if (turbulent_velocity) then
                             ! Need to make sure v_t doesn't go down because of rho leaving the cell...
                             ctv = uold(ind_blast(iSN),iturbvel)/d
!!$                             print *,"ave_SN: orig v_t (km/s)", ctv * scale_v/1.0d5
                             uold(ind_blast(iSN),iturbvel) = ctv * (d - mload/vol_loc) ! iprist with new cell rho
                          end if
                       endif
!!                       print *,"ave_SN: cell rho start:", d
                       d=uold(ind_blast(iSN),1)-mload/vol_loc ! Very important! Updates the cell density - RS
!!                       print *,"ave_SN: cell rho end  :", d, ind_blast(iSN)
                       uold(ind_blast(iSN),1)=d
                       uold(ind_blast(iSN),2)=d*u
                       uold(ind_blast(iSN),3)=d*v
                       uold(ind_blast(iSN),4)=d*w
                       uold(ind_blast(iSN),5)=eint+0.5d0*d*(u*u+v*v+w*w)

                       vloadSN(iSN,1)=(mSN(iSN)*vSN(iSN,1)+mload*u)/mloadSN(iSN)
                       vloadSN(iSN,2)=(mSN(iSN)*vSN(iSN,2)+mload*v)/mloadSN(iSN)
                       vloadSN(iSN,3)=(mSN(iSN)*vSN(iSN,3)+mload*w)/mloadSN(iSN)
                    endif
                 end do
              endif
           end do
           
        end do
        ! End loop over cells
     end do
     ! End loop over grids
  end do
  ! End loop over levels

  !################################################################
#ifndef WITHOUTMPI
  vol_gas_mpi=0d0; dq_mpi=0d0; u2Blast_mpi=0d0; mloadSN_mpi=0d0; ZloadSN_mpi=0d0; Z3loadSN_mpi=0d0; vloadSN_mpi=0d0
  ! Put the nSN size arrays into nSN_tot size arrays to synchronize processors
  do iSN=1,nSN
     ind_SN=iSN_myid(iSN)
     vol_gas_mpi(ind_SN)=vol_gas(iSN)
     mloadSN_mpi(ind_SN)=mloadSN(iSN)
     ZloadSN_mpi(ind_SN)=ZloadSN(iSN)
     Z3loadSN_mpi(ind_SN)=Z3loadSN(iSN) ! - RS
     vloadSN_mpi(ind_SN,1)=vloadSN(iSN,1)
     vloadSN_mpi(ind_SN,2)=vloadSN(iSN,2)
     vloadSN_mpi(ind_SN,3)=vloadSN(iSN,3)
     dq_mpi     (ind_SN,1)=dq     (iSN,1)
     dq_mpi     (ind_SN,2)=dq     (iSN,2)
     dq_mpi     (ind_SN,3)=dq     (iSN,3)
     u2Blast_mpi(ind_SN,1)=u2Blast(iSN,1)
     u2Blast_mpi(ind_SN,2)=u2Blast(iSN,2)
     u2Blast_mpi(ind_SN,3)=u2Blast(iSN,3)
  enddo
  call MPI_ALLREDUCE(vol_gas_mpi,vol_gas_all,nSN_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(mloadSN_mpi,mloadSN_all,nSN_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(ZloadSN_mpi,ZloadSN_all,nSN_tot  ,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(Z3loadSN_mpi,Z3loadSN_all,nSN_tot,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info) ! - RS
  call MPI_ALLREDUCE(vloadSN_mpi,vloadSN_all,nSN_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(dq_mpi     ,dq_all     ,nSN_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  call MPI_ALLREDUCE(u2Blast_mpi,u2Blast_all,nSN_tot*3,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,info)
  vol_gas_mpi=vol_gas_all
  mloadSN_mpi=mloadSN_all
  ZloadSN_mpi=ZloadSN_all
  Z3loadSN_mpi=Z3loadSN_all ! - RS
  vloadSN_mpi=vloadSN_all
  dq_mpi     =dq_all
  u2Blast_mpi=u2Blast_all
  ! Put the nSN_tot size arrays into nSN size arrays
  do iSN=1,nSN
     ind_SN=iSN_myid(iSN)
     vol_gas(iSN)=vol_gas_mpi(ind_SN)
     mloadSN(iSN)=mloadSN_mpi(ind_SN)
     ZloadSN(iSN)=ZloadSN_mpi(ind_SN)
     Z3loadSN(iSN)=Z3loadSN_mpi(ind_SN) ! - RS
     vloadSN(iSN,1)=vloadSN_mpi(ind_SN,1)
     vloadSN(iSN,2)=vloadSN_mpi(ind_SN,2)
     vloadSN(iSN,3)=vloadSN_mpi(ind_SN,3)
     dq     (iSN,1)=dq_mpi     (ind_SN,1)
     dq     (iSN,2)=dq_mpi     (ind_SN,2)
     dq     (iSN,3)=dq_mpi     (ind_SN,3)
     u2Blast(iSN,1)=u2Blast_mpi(ind_SN,1)
     u2Blast(iSN,2)=u2Blast_mpi(ind_SN,2)
     u2Blast(iSN,3)=u2Blast_mpi(ind_SN,3)
  enddo
#endif
  !################################################################
  ! At this point ekBlast is vol_loc
  do iSN=1,nSN
     if(vol_gas(iSN)>0d0)then
        dq(iSN,1)=dq(iSN,1)/vol_gas(iSN)
        dq(iSN,2)=dq(iSN,2)/vol_gas(iSN)
        dq(iSN,3)=dq(iSN,3)/vol_gas(iSN)
        u2Blast(iSN,1)=u2Blast(iSN,1)/vol_gas(iSN)
        u2Blast(iSN,2)=u2Blast(iSN,2)/vol_gas(iSN)
        u2Blast(iSN,3)=u2Blast(iSN,3)/vol_gas(iSN)
        u2=u2Blast(iSN,1)-dq(iSN,1)**2
        v2=u2Blast(iSN,2)-dq(iSN,2)**2
        w2=u2Blast(iSN,3)-dq(iSN,3)**2
        ekBlast(iSN)=max(0.5d0*(u2+v2+w2),0.0d0)
        if (ekBlast(iSN).le.1d-5) ekBlast(iSN)=0d0
     endif
  end do

  if(verbose)write(*,*)'Exiting average_SN'

end subroutine average_SN



!################################################################
!################################################################
!################################################################
!################################################################
subroutine Sedov_blast(xSN,mSN,indSN,vol_gas,dq,ekBlast,nSN,mloadSN,ZloadSN,Z3loadSN,vloadSN) ! MTS
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !------------------------------------------------------------------------
  ! 
  !------------------------------------------------------------------------
  integer::ilevel,j,iSN,nSN,ind,ix,iy,iz,ngrid,iskip
  integer::i,nx_loc,igrid,ncache
  integer,dimension(1:nvector),save::ind_grid,ind_cell
!#ifdef SOLVERhydro
!  integer ::imetal=6
!#endif
!#ifdef SOLVERmhd
!  integer ::imetal=9
!#endif
  real(dp)::x,y,z,dx,dxx,dyy,dzz,dr_SN,u,v,w,d_gas,ESN
  real(dp)::scale,dx_min,dx_loc,vol_loc,rmax2,rmax, ctv
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  ! RS - Adding the ave Z3loadSN mass loading for primordialy generated Z
  real(dp),dimension(1:nSN)::mSN,p_gas,vol_gas,uSedov,ekBlast,mloadSN,ZloadSN,Z3loadSN
  real(dp),dimension(1:nSN,1:3)::xSN,dq,vloadSN
  integer ,dimension(1:nSN)::indSN
  logical ,dimension(1:nvector),save::ok

  if(verbose)write(*,*)'Entering Sedov_blast'

  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_min=scale*0.5D0**nlevelmax

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Maximum radius of the ejecta
  rmax=MAX(1.5d0*dx_min*scale_l/aexp,rbubble*3.08d18)
  rmax=rmax/scale_l ! rmax = rbubble converted to cm converted to code units
  rmax2=rmax*rmax
  
  ! Ejecta specific energy (accounting for dilution)
  ! RS - Need a new array passed into Sedov to account for differing
  ! energy for the different types of SN... 
  ESN=(1d51/(10d0*2d33))/scale_v**2

  do iSN=1,nSN
     if(vol_gas(iSN)>0d0)then       ! vol_gas is the total vol of gas in all cells in the blast radius
        d_gas=mSN(iSN)/vol_gas(iSN) ! d_gas is fraction of SN eject mass to ALL cells in the blast radius
        if(ekBlast(iSN)==0d0)then
           p_gas (iSN)=d_gas*ESN    ! RS - here's where we'd use a new ESN array - specific to SN type
!           print*, "Sedov: pgas and d_gas",p_gas,d_gas
           uSedov(iSN)=0d0
        else
           p_gas (iSN)=(1d0-f_ek)*d_gas*ESN
           uSedov(iSN)=sqrt(f_ek*2.0*mSN(iSN)*ESN/ekBlast(iSN)/mloadSN(iSN))
        endif
     else
        p_gas(iSN)=mSN(iSN)*ESN/ekBlast(iSN)
     endif
  end do

  ! Loop over levels
  do ilevel=levelmin,nlevelmax
     ! Computing local volume (important for averaging hydro quantities) 
     dx=0.5D0**ilevel 
     dx_loc=dx*scale
     vol_loc=dx_loc**ndim
     ! Cells center position relative to grid center position
     do ind=1,twotondim  
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
     end do

     ! Loop over grids
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do

        ! Loop over cells
        do ind=1,twotondim  
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do

           ! Flag leaf cells
           do i=1,ngrid
              ok(i)=son(ind_cell(i))==0
           end do

           do i=1,ngrid
              if(ok(i))then
                 ! Get gas cell position
                 x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                 y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                 z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
                 do iSN=1,nSN
                    ! Check if the cell lies within the SN radius
                    dxx=x-xSN(iSN,1)
                    dyy=y-xSN(iSN,2)
                    dzz=z-xSN(iSN,3)
                    dr_SN=dxx**2+dyy**2+dzz**2
                    if(dr_SN.lt.rmax2)then ! If we're within the max radius of the blast/bubble... - RS
                       ! vol_gas(iSN) is the total gas volume within the blast radius
                       ! mloadSN is mass of SN and gas carried along from cell
                       d_gas=mloadSN(iSN)/vol_gas(iSN) ! d_gas is density fraction of mloadSN going into a single cell
!!$                       print *,"Sedov_blast: a = ",aexp
!!$                       print *,"Sedov_blast: z = ",1.0/aexp - 1.0
!!$                       print *,"Sedov_blast: cell index ",ind_cell(i)
!!$                       print *,"Sedov_blast: cell starting rho ",uold(ind_cell(i),1)
!!$                       print *,"Sedov_blast: rho incoming (d_gas) ",d_gas

                       ! So we're dividing up the mloadSN equally into the cell's within the blast radius
                       ! Compute the density and the metal density of the cell

                       if (turbulent_velocity) then
                          ctv = uold(ind_cell(i),iturbvel)/uold(ind_cell(i),1) ! current v_t
!!$                          print *,"Sedov_blast: v_t (km/s) before d inc ", ctv * scale_v/1.0d5
                       end if

                       ! ------------------
                       ! NEW GAS ADDED HERE
                       ! ------------------
                       uold(ind_cell(i),1) = uold(ind_cell(i),1)+d_gas 

                       if (turbulent_velocity) then
                          uold(ind_cell(i),iturbvel) = ctv * uold(ind_cell(i),1)
!!$                          print *,"Sedov_blast: v_t (km/s) after inc fixed ", uold(ind_cell(i),iturbvel)/uold(ind_cell(i),1) * scale_v/1.0d5
                       end if

                       ! Update metallicity
                       if(metal)uold(ind_cell(i),imetal)=uold(ind_cell(i),imetal)+d_gas*ZloadSN(iSN)
                       ! MTS
!!                       if(rpgas)uold(ind_cell(i),irpgas)=uold(ind_cell(i),irpgas)+d_gas*rploadSN(iSN)

                       if(prist_gas_fraction)then
                          ! Pristine fraction
                          ! Since the cell's density goes up due to the injection of mass,
                          ! some of which is new Z, the pristine fraction (iprist/rho) will 
                          ! go down since rho has increased. Remember, iprist is a mass density.
                          ! So, nothing to do for iprist
!!$                          print *,"Sedov_blast: updated Z ",uold(ind_cell(i),imetal)/uold(ind_cell(i),1)
!!$                          print *,"Sedov_blast: updated P ",uold(ind_cell(i),iprist)/uold(ind_cell(i),1)

!!$                          uold(ind_cell(i),iprist) = uold(ind_cell(i),iprist) - d_gas*(1.0d0-ZloadSN(iSN))

                          ! Update the primordial Z mass density in the cell due to injected
                          ! metals from POP III stars. (Z3loadSN is computed above...)
                          uold(ind_cell(i),iprimordz)=uold(ind_cell(i),iprimordz)+d_gas*Z3loadSN(iSN)
                       endif
                       ! Velocity at a given dr_SN linearly interpolated between zero and uSedov
                       u=uSedov(iSN)*(dxx/rmax-dq(iSN,1))+vloadSN(iSN,1)
                       v=uSedov(iSN)*(dyy/rmax-dq(iSN,2))+vloadSN(iSN,2)
                       w=uSedov(iSN)*(dzz/rmax-dq(iSN,3))+vloadSN(iSN,3)
                       ! Add each momentum component of the blast wave to the gas
                       uold(ind_cell(i),2)=uold(ind_cell(i),2)+d_gas*u
                       uold(ind_cell(i),3)=uold(ind_cell(i),3)+d_gas*v
                       uold(ind_cell(i),4)=uold(ind_cell(i),4)+d_gas*w
                       ! Finally update the total energy of the gas
                       uold(ind_cell(i),5)=uold(ind_cell(i),5)+0.5*d_gas*(u*u+v*v+w*w)+p_gas(iSN)
                    endif
                 end do
              endif
           end do
           
        end do
        ! End loop over cells
     end do
     ! End loop over grids
  end do
  ! End loop over levels

  do iSN=1,nSN
     if(vol_gas(iSN)==0d0)then
        d_gas=mloadSN(iSN)/ekBlast(iSN)
        u=vloadSN(iSN,1)
        v=vloadSN(iSN,2)
        w=vloadSN(iSN,3)
        if(indSN(iSN)>0)then
           print *,"Sedov_blast: NO gas in SN blast radius!, rho ratio old/new ",uold(indSN(iSN),1)/(uold(indSN(iSN),1)+d_gas)
           if (turbulent_velocity) then
              ctv = uold(indSN(iSN),iturbvel)/uold(indSN(iSN),1)
           end if
           uold(indSN(iSN),1)=uold(indSN(iSN),1)+d_gas
           uold(indSN(iSN),2)=uold(indSN(iSN),2)+d_gas*u
           uold(indSN(iSN),3)=uold(indSN(iSN),3)+d_gas*v
           uold(indSN(iSN),4)=uold(indSN(iSN),4)+d_gas*w
           uold(indSN(iSN),5)=uold(indSN(iSN),5)+d_gas*0.5*(u*u+v*v+w*w)+p_gas(iSN)
           if(metal)uold(indSN(iSN),imetal)=uold(indSN(iSN),imetal)+d_gas*ZloadSN(iSN)
!!           if(rpgas)uold(ind_cell(i),irpgas)=uold(ind_cell(i),irpgas)+d_gas*rploadSN(iSN) ! MTS
           ! Rick Sarmento - TODO
           ! We're looping over the SNe... adding the mass loading to the cells
           ! THIS IS ONLY DONE if the cell's gas volume is 0.0 ... 
           if(prist_gas_fraction) then
              ! Pristine fraction
              ! Since the cell's density goes up due to the injection of mass,
              ! some of which is new Z, the pristine fraction (iprist/rho) will 
              ! go down since rho has increased. Remember, iprist is a mass density.
              ! So, nothing to do for iprist
              ! print *,"Sedov_blast: NOG d_gas:", d_gas
              ! print *,"Sedov_blast: NOG PGF:", uold(indSN(iSN),iprist)/uold(indSN(iSN),1)
              ! print *,"Sedov_blast: NOG Z/(1-PGF) new:", (uold(indSN(iSN),imetal))/(uold(indSN(iSN),1)-uold(indSN(iSN),iprist))
              ! Primordial Z
              ! Update the primordial Z mass density in the cell due to injected
              ! metals from POP III stars. (Z3loadSN is computed above...)
              uold(indSN(iSN),iprimordz)=uold(indSN(iSN),iprimordz)+d_gas*Z3loadSN(iSN) 
!!$              print *,"SB, vol_gas = 0, d_gas    =",d_gas
!!$              print *,"SB, vol_gas = 0, Z3loadSN =",Z3loadSN(iSN)
!!$              print *,"SB, vol_gas = 0, new iprimordz =",uold(indSN(iSN),iprimordz)
!!$              print *,"SB, vol_gas = 0, imetal        =",uold(indSN(iSN),imetal)
           endif
           if (turbulent_velocity) then
              uold(indSN(iSN),iturbvel) = ctv * uold(indSN(iSN),1)              
           end if
        endif
     endif
  end do

  if(verbose)write(*,*)'Exiting Sedov_blast'

end subroutine Sedov_blast

! ************************************************************************
! ************************ Sedov_blast_nsb **********************
! ************************************************************************
subroutine Sedov_blast_NSB(xNSB,mNSB,indNSB,vol_gas,dq,ekBlast,nNSB,mloadNSB,vloadNSB, rploadNSB)
  use pm_commons
  use amr_commons
  use hydro_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  !------------------------------------------------------------------------
  ! 
  !------------------------------------------------------------------------
  integer::ilevel,j,iNSB,nNSB,ind,ix,iy,iz,ngrid,iskip
  integer::i,nx_loc,igrid,ncache
  integer,dimension(1:nvector),save::ind_grid,ind_cell
!#ifdef SOLVERhydro
!  integer ::imetal=6
!#endif
!#ifdef SOLVERmhd
!  integer ::imetal=9
!#endif
  real(dp)::x,y,z,dx,dxx,dyy,dzz,dr_NSB,u,v,w,d_gas,ENSB
  real(dp)::scale,dx_min,dx_loc,vol_loc,rmax2,rmax, ctv
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp),dimension(1:3)::skip_loc
  real(dp),dimension(1:twotondim,1:3)::xc
  real(dp),dimension(1:nNSB)::mNSB,p_gas,vol_gas,uSedov,ekBlast,mloadNSB,rploadNSB !MTS
  real(dp),dimension(1:nNSB,1:3)::xNSB,dq,vloadNSB
  integer ,dimension(1:nNSB)::indNSB
  logical ,dimension(1:nvector),save::ok

  if(verbose)write(*,*)'Entering Sedov_blast'
  
  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_min=scale*0.5D0**nlevelmax

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Maximum radius of the ejecta
  rmax=MAX(1.5d0*dx_min*scale_l/aexp,rbubble*3.08d18)
  rmax=rmax/scale_l ! rmax = rbubble converted to cm converted to code units
  rmax2=rmax*rmax
  
  ! Ejecta specific energy (accounting for dilution)
  ! The NSB merger energy is 1e51 per event. Each event is based on 1e-3 M_sun
  ! of ejecta... so we want that much energy for that much mass... 
!!  ENSB=(1d51/(10d0*2d33))/scale_v**2
  ENSB = (1d51/(0.001*2d33))/scale_v**2

  do iNSB=1,nNSB
     if(vol_gas(iNSB)>0d0)then       ! vol_gas is the total vol of gas in all cells in the blast radius
        d_gas=mNSB(iNSB)/vol_gas(iNSB) ! d_gas is fraction of NSB eject mass to ALL cells in the blast radius
        if(ekBlast(iNSB)==0d0)then
           p_gas (iNSB)=d_gas*ENSB    ! RS - here's where we'd use a new ENSB array - specific to NSB type
           uSedov(iNSB)=0d0
        else
           p_gas (iNSB)=(1d0-f_ek)*d_gas*ENSB
           uSedov(iNSB)=sqrt(f_ek*2.0*mNSB(iNSB)*ENSB/ekBlast(iNSB)/mloadNSB(iNSB))
        endif
     else
        p_gas(iNSB)=mNSB(iNSB)*ENSB/ekBlast(iNSB)
     endif
  end do

  ! Loop over levels
  do ilevel=levelmin,nlevelmax
     ! Computing local volume (important for averaging hydro quantities) 
     dx=0.5D0**ilevel 
     dx_loc=dx*scale
     vol_loc=dx_loc**ndim
     ! Cells center position relative to grid center position
     do ind=1,twotondim  
        iz=(ind-1)/4
        iy=(ind-1-4*iz)/2
        ix=(ind-1-2*iy-4*iz)
        xc(ind,1)=(dble(ix)-0.5D0)*dx
        xc(ind,2)=(dble(iy)-0.5D0)*dx
        xc(ind,3)=(dble(iz)-0.5D0)*dx
     end do

     ! Loop over grids
     ncache=active(ilevel)%ngrid
     do igrid=1,ncache,nvector
        ngrid=MIN(nvector,ncache-igrid+1)
        do i=1,ngrid
           ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
        end do

        ! Loop over cells
        do ind=1,twotondim  
           iskip=ncoarse+(ind-1)*ngridmax
           do i=1,ngrid
              ind_cell(i)=iskip+ind_grid(i)
           end do

           ! Flag leaf cells
           do i=1,ngrid
              ok(i)=son(ind_cell(i))==0
           end do

           do i=1,ngrid
              if(ok(i))then
                 ! Get gas cell position
                 x=(xg(ind_grid(i),1)+xc(ind,1)-skip_loc(1))*scale
                 y=(xg(ind_grid(i),2)+xc(ind,2)-skip_loc(2))*scale
                 z=(xg(ind_grid(i),3)+xc(ind,3)-skip_loc(3))*scale
                 do iNSB=1,nNSB
                    ! Check if the cell lies within the NSB radius
                    dxx=x-xNSB(iNSB,1)
                    dyy=y-xNSB(iNSB,2)
                    dzz=z-xNSB(iNSB,3)
                    dr_NSB=dxx**2+dyy**2+dzz**2
                    if(dr_NSB.lt.rmax2)then ! If we're within the max radius of the blast/bubble... - RS
                       ! vol_gas(iNSB) is the total gas volume within the blast radius
                       ! mloadNSB is mass of NSB and gas carried along from cell
                       d_gas=mloadNSB(iNSB)/vol_gas(iNSB) ! d_gas is density fraction of mloadNSB going into a single cell
                       ! So we're dividing up the mloadNSB equally into the cell's within the blast radius
                       ! Compute the density and the metal density of the cell

                       ! ------------------
                       ! NEW GAS ADDED HERE
                       ! ------------------
                       uold(ind_cell(i),1) = uold(ind_cell(i),1)+d_gas 

                       if(rpgas) then
                          uold(ind_cell(i),irpgas)=uold(ind_cell(i),irpgas)+d_gas*rploadNSB(iNSB)
!                          print *,"Sedov_NSB: updating irpgas ", rploadNSB(iNSB) ! DEBUG - RS
!                          print *,"Sedov_NSB: updating irpgas udpated ", uold(ind_cell(i),irpgas)
                       end if
                       ! Velocity at a given dr_NSB linearly interpolated between zero and uSedov
                       u=uSedov(iNSB)*(dxx/rmax-dq(iNSB,1))+vloadNSB(iNSB,1)
                       v=uSedov(iNSB)*(dyy/rmax-dq(iNSB,2))+vloadNSB(iNSB,2)
                       w=uSedov(iNSB)*(dzz/rmax-dq(iNSB,3))+vloadNSB(iNSB,3)
                       ! Add each momentum component of the blast wave to the gas
                       uold(ind_cell(i),2)=uold(ind_cell(i),2)+d_gas*u
                       uold(ind_cell(i),3)=uold(ind_cell(i),3)+d_gas*v
                       uold(ind_cell(i),4)=uold(ind_cell(i),4)+d_gas*w
                       ! Finally update the total energy of the gas
                       uold(ind_cell(i),5)=uold(ind_cell(i),5)+0.5*d_gas*(u*u+v*v+w*w)+p_gas(iNSB)
                    endif
                 end do
              endif
           end do
           
        end do
        ! End loop over cells
     end do
     ! End loop over grids
  end do
  ! End loop over levels

  do iNSB=1,nNSB
     if(vol_gas(iNSB)==0d0)then
        d_gas=mloadNSB(iNSB)/ekBlast(iNSB)
        u=vloadNSB(iNSB,1)
        v=vloadNSB(iNSB,2)
        w=vloadNSB(iNSB,3)
        if(indNSB(iNSB)>0)then
           uold(indNSB(iNSB),1)=uold(indNSB(iNSB),1)+d_gas
           uold(indNSB(iNSB),2)=uold(indNSB(iNSB),2)+d_gas*u
           uold(indNSB(iNSB),3)=uold(indNSB(iNSB),3)+d_gas*v
           uold(indNSB(iNSB),4)=uold(indNSB(iNSB),4)+d_gas*w
           uold(indNSB(iNSB),5)=uold(indNSB(iNSB),5)+d_gas*0.5*(u*u+v*v+w*w)+p_gas(iNSB)
           if(rpgas)uold(ind_cell(i),irpgas)=uold(ind_cell(i),irpgas)+d_gas*rploadNSB(iNSB) ! MTS
        endif
     endif
  end do

  if(verbose)write(*,*)'Exiting Sedov_blast'

end subroutine Sedov_blast_NSB

! ************************* End Sedov_blast_nsb ******************

!################################################################
!################################################################
!################################################################
!################################################################
subroutine getSNonmyid(iSN_myid,nSN_myid,xSN,nSN)
  use amr_commons
  use pm_commons
  implicit none
#ifndef WITHOUTMPI
  include 'mpif.h'
#endif
  integer,dimension(1:nSN)::iSN_myid
  integer::nSN_myid,ii,nSN
  integer,dimension(1:8)::idom,jdom,kdom,cpu_min,cpu_max
  integer::lmin,iSN,nx_loc,ilevel,lmax,bit_length,maxdom,icpu
  integer::imin,jmin,kmin,imax,jmax,kmax,ndom,impi,i,j,k,ncpu_read
  integer,dimension(1:ncpu)::cpu_list
  logical,dimension(1:ncpu)::cpu_read
  real(dp)::scale,dx,dx_min,drSN
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  real(dp),dimension(1:3)::skip_loc
  real(dp)::xxmin,yymin,zzmin,xxmax,yymax,zzmax,dmax
  real(dp),dimension(1:nSN,1:3)::xSN
  real(qdp),dimension(1:8)::bounding_min,bounding_max
  real(qdp),dimension(1:1)::order_min ! R. Sarmento - hilbert3d expects a double prec array for order min.
  !  real(qdp)::dkey,order_min,oneqdp=1.0
  real(qdp)::dkey,oneqdp=1.0
  
  ! Mesh spacing in that level
  nx_loc=(icoarse_max-icoarse_min+1)
  skip_loc=(/0.0d0,0.0d0,0.0d0/)
  skip_loc(1)=dble(icoarse_min)
  skip_loc(2)=dble(jcoarse_min)
  skip_loc(3)=dble(kcoarse_min)
  scale=boxlen/dble(nx_loc)
  dx_min=scale*0.5D0**nlevelmax

  ! Conversion factor from user units to cgs units
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Maximum radius of the ejecta
  drSN=2d0*MAX(1.5d0*dx_min*scale_l/aexp,rbubble*3.08d18)
  drSN=drSN/scale_l

  !-----------------------
  ! Map parameters
  !-----------------------
  lmax=nlevelmax
  iSN_myid=0
  ii=0
  do iSN=1,nSN

     cpu_read=.false.
     ! Compute boundaries for the SN cube of influence
     xxmin=(xSN(iSN,1)-drSN)/scale ; xxmax=(xSN(iSN,1)+drSN)/scale
     yymin=(xSN(iSN,2)-drSN)/scale ; yymax=(xSN(iSN,2)+drSN)/scale
     zzmin=(xSN(iSN,3)-drSN)/scale ; zzmax=(xSN(iSN,3)+drSN)/scale

     if(TRIM(ordering).eq.'hilbert')then
        
        dmax=max(xxmax-xxmin,yymax-yymin,zzmax-zzmin)
        do ilevel=1,lmax
           dx=0.5d0**ilevel
           if(dx.lt.dmax)exit
        end do
        lmin=ilevel
        bit_length=lmin-1
        maxdom=2**bit_length
        imin=0; imax=0; jmin=0; jmax=0; kmin=0; kmax=0
        if(bit_length>0)then
           imin=int(xxmin*dble(maxdom))
           imax=imin+1
           jmin=int(yymin*dble(maxdom))
           jmax=jmin+1
           kmin=int(zzmin*dble(maxdom))
           kmax=kmin+1
        endif
        
        dkey=(real(2**(nlevelmax+1),kind=qdp)/real(maxdom,kind=qdp))**ndim
        ndom=1
        if(bit_length>0)ndom=8
        idom(1)=imin; idom(2)=imax
        idom(3)=imin; idom(4)=imax
        idom(5)=imin; idom(6)=imax
        idom(7)=imin; idom(8)=imax
        jdom(1)=jmin; jdom(2)=jmin
        jdom(3)=jmax; jdom(4)=jmax
        jdom(5)=jmin; jdom(6)=jmin
        jdom(7)=jmax; jdom(8)=jmax
        kdom(1)=kmin; kdom(2)=kmin
        kdom(3)=kmin; kdom(4)=kmin
        kdom(5)=kmax; kdom(6)=kmax
        kdom(7)=kmax; kdom(8)=kmax
        
        do i=1,ndom
           if(bit_length>0)then
              call hilbert3d(idom(i),jdom(i),kdom(i),order_min,bit_length,1)
           else
              order_min(1)=0.0d0
           endif
           bounding_min(i)=(order_min(1))*dkey
           bounding_max(i)=(order_min(1)+oneqdp)*dkey
        end do
        
        cpu_min=0; cpu_max=0
        do impi=1,ncpu
           do i=1,ndom
              if (   bound_key(impi-1).le.bounding_min(i).and.&
                   & bound_key(impi  ).gt.bounding_min(i))then
                 cpu_min(i)=impi
              endif
              if (   bound_key(impi-1).lt.bounding_max(i).and.&
                   & bound_key(impi  ).ge.bounding_max(i))then
                 cpu_max(i)=impi
              endif
           end do
        end do
        
        ncpu_read=0
        do i=1,ndom
           do j=cpu_min(i),cpu_max(i)
              if(.not. cpu_read(j))then
                 ncpu_read=ncpu_read+1
                 cpu_list(ncpu_read)=j
                 cpu_read(j)=.true.
              endif
           enddo
        enddo
     else
        ncpu_read=ncpu
        do j=1,ncpu
           cpu_list(j)=j
        end do
     end  if
     
     ! Create the index array for SN in processor myid
     do k=1,ncpu_read
        icpu=cpu_list(k)
        if(icpu==myid)then
           ii=ii+1
           iSN_myid(ii)=iSN
        endif
     enddo

  enddo
  
  ! Number of SN in processor myid
  nSN_myid=ii

end subroutine getSNonmyid
!################################################################
!################################################################
!################################################################
!################################################################
